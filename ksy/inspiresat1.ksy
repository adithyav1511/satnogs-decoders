---
meta:
  id: inspiresat1
  title: INSPIRESat-1 70cm telemetry decoder
  endian: be
doc: |
  :field dest_callsign: ax25_frame.ax25_header.dest_callsign_raw.callsign_ror.callsign
  :field src_callsign: ax25_frame.ax25_header.src_callsign_raw.callsign_ror.callsign
  :field src_ssid: ax25_frame.ax25_header.src_ssid_raw.ssid
  :field dest_ssid: ax25_frame.ax25_header.dest_ssid_raw.ssid
  :field ctl: ax25_frame.ax25_header.ctl
  :field pid: ax25_frame.payload.pid
  :field ccsds_version: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.ccsds_version
  :field packet_type: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.packet_type
  :field secondary_header_flag: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.secondary_header_flag
  :field is_stored_data: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.is_stored_data
  :field application_process_id: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.application_process_id
  :field grouping_flag: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.grouping_flag
  :field sequence_count: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.sequence_count
  :field packet_length: ax25_frame.payload.ax25_info.ccsds_space_packet.packet_primary_header.packet_length
  :field time_stamp_seconds: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.secondary_header.time_stamp_seconds
  :field sub_seconds: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.secondary_header.sub_seconds
  :field padding: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.secondary_header.padding
  :field _VERSION: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..VERSION
  :field _TYPE: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..TYPE
  :field _SEC_HDR_FLAG: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..SEC_HDR_FLAG
  :field _PKT_APID: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..PKT_APID
  :field _SEQ_FLGS: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..SEQ_FLGS
  :field _SRC_SEQ_CTR: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..SRC_SEQ_CTR
  :field _PKT_LEN: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..PKT_LEN
  :field _SHCOARSE: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..SHCOARSE
  :field _SHFINE: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..SHFINE
  :field _cmd_recv_count: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_recv_count
  :field _cmd_fail_count: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_fail_count
  :field _cmd_succ_count: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_succ_count
  :field _cmd_succ_op: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_succ_op
  :field _cmd_fail_op: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_fail_op
  :field _cmd_fail_code: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_fail_code
  :field _pwr_status: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status
  :field _eclipse_state: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..eclipse_state
  :field _pwr_status_sd1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_sd1
  :field _pwr_status_sd0: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_sd0
  :field _pwr_status_htr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_htr
  :field _pwr_status_sband: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_sband
  :field _pwr_status_adcs: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_adcs
  :field _pwr_status_cip: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_cip
  :field _pwr_status_daxss: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pwr_status_daxss
  :field _sd_read_misc: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_read_misc
  :field _sd_read_scic: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_read_scic
  :field _sd_read_scid: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_read_scid
  :field _sd_read_adcs: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_read_adcs
  :field _sd_read_beacon: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_read_beacon
  :field _sd_read_log: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_read_log
  :field _sd_write_misc: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_write_misc
  :field _sd_write_scic: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_write_scic
  :field _sd_write_scid: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_write_scid
  :field _sd_write_adcs: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_write_adcs
  :field _sd_write_beacon: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_write_beacon
  :field _sd_write_log: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sd_write_log
  :field _cmd_loss_timer: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cmd_loss_timer
  :field _alive_flags: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_flags
  :field _clt_state: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..clt_state
  :field _alive_daxss: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_daxss
  :field _alive_cip: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_cip
  :field _alive_adcs: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_adcs
  :field _alive_sband: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_sband
  :field _alive_uhf: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_uhf
  :field _alive_sd1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_sd1
  :field _alive_sd0: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..alive_sd0
  :field _cip_comstat: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cip_comstat
  :field _cip_temp1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cip_temp1
  :field _cip_temp2: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cip_temp2
  :field _cip_temp3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cip_temp3
  :field _uhf_temp_buff: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_temp_buff
  :field _uhf_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_temp
  :field _uhf_config: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_config
  :field _uhf_locked: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_locked
  :field _uhf_readback: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_readback
  :field _uhf_swd: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_swd
  :field _uhf_afc: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_afc
  :field _uhf_echo: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_echo
  :field _uhf_channel: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_channel
  :field _sband_pa_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_pa_curr
  :field _sband_pa_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_pa_volt
  :field _sband_rf_pwr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_rf_pwr
  :field _sband_pa_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_pa_temp
  :field _sband_top_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_top_temp
  :field _sband_bottom_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_bottom_temp
  :field _adcs_cmd_acpt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_cmd_acpt
  :field _adcs_cmd_fail: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_cmd_fail
  :field _adcs_time: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_time
  :field _adcs_info: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_info
  :field _adcs_att_valid: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_att_valid
  :field _adcs_refs_valid: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_refs_valid
  :field _adcs_time_valid: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_time_valid
  :field _adcs_mode: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_mode
  :field _adcs_recom_sun_pt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_recom_sun_pt
  :field _adcs_sun_pt_state: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_sun_pt_state
  :field _adcs_star_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_star_temp
  :field _adcs_wheel_temp1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_wheel_temp1
  :field _adcs_wheel_temp2: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_wheel_temp2
  :field _adcs_wheel_temp3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_wheel_temp3
  :field _adcs_digi_bus_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_digi_bus_volt
  :field _adcs_sun_vec1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_sun_vec1
  :field _adcs_sun_vec2: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_sun_vec2
  :field _adcs_sun_vec3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_sun_vec3
  :field _adcs_wheel_sp1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_wheel_sp1
  :field _adcs_wheel_sp2: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_wheel_sp2
  :field _adcs_wheel_sp3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_wheel_sp3
  :field _adcs_body_rt1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_body_rt1
  :field _adcs_body_rt2: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_body_rt2
  :field _adcs_body_rt3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_body_rt3
  :field _pad1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pad1
  :field _pad2: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pad2
  :field _pad3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..pad3
  :field _daxss_time_sec: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_time_sec
  :field _daxss_cmd_op: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_cmd_op
  :field _daxss_cmd_succ: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_cmd_succ
  :field _daxss_cmd_fail: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_cmd_fail
  :field _daxss_cdh_enables: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_cdh_enables
  :field _daxss_cdh_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_cdh_temp
  :field _daxss_sps_rate: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_sps_rate
  :field _daxss_sps_x: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_sps_x
  :field _daxss_sps_y: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_sps_y
  :field _daxss_slow_count: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_slow_count
  :field _bat_fg1: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat_fg1
  :field _daxss_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_curr
  :field _daxss_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..daxss_volt
  :field _cdh_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cdh_curr
  :field _cdh_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cdh_volt
  :field _sband_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_curr
  :field _sband_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sband_volt
  :field _uhf_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_curr
  :field _uhf_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..uhf_volt
  :field _heater_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..heater_curr
  :field _heater_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..heater_volt
  :field _sp2_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp2_curr
  :field _sp2_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp2_volt
  :field _sp1_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp1_curr
  :field _sp1_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp1_volt
  :field _sp0_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp0_curr
  :field _sp0_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp0_volt
  :field _bat_vcell: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat_vcell
  :field _gps_12v_2_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..gps_12v_2_curr
  :field _gps_12v_2_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..gps_12v_2_volt
  :field _gps_12v_1_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..gps_12v_1_curr
  :field _gps_12v_1_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..gps_12v_1_volt
  :field _bat_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat_curr
  :field _bat_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat_volt
  :field _adcs_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_curr
  :field _adcs_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..adcs_volt
  :field _3p3_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..3p3_curr
  :field _3p3_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..3p3_volt
  :field _cip_curr: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cip_curr
  :field _cip_volt: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..cip_volt
  :field _obc_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..obc_temp
  :field _eps_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..eps_temp
  :field _int_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..int_temp
  :field _sp0_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp0_temp
  :field _bat0_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat0_temp
  :field _sp1_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp1_temp
  :field _bat1_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat1_temp
  :field _sp2_temp: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..sp2_temp
  :field _bat_fg3: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat_fg3
  :field _bat0_temp_conv: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat0_temp_conv
  :field _bat1_temp_conv: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..bat1_temp_conv
  :field _mode: ax25_frame.payload.ax25_info.ccsds_space_packet.data_section.user_data_field..mode

seq:
  - id: ax25_frame
    type: ax25_frame
    doc-ref: 'https://www.tapr.org/pub_ax25.html'
types:
  ax25_frame:
    seq:
      - id: ax25_header
        type: ax25_header
      - id: payload
        type: i_frame
          #switch-on: ax25_header.ctl & 0x13
          #cases:
           #0x03: ui_frame
           #0x13: ui_frame
           #0x00: i_frame
           #0x02: i_frame
           #0x10: i_frame
           #0x12: i_frame
           #0x11: s_frame
  ax25_header:
    seq:
      - id: dest_callsign_raw
        type: callsign_raw
      - id: dest_ssid_raw
        type: ssid_mask
      - id: src_callsign_raw
        type: callsign_raw
      - id: src_ssid_raw
        type: ssid_mask
      - id: ctl
        type: u1
  callsign_raw:
    seq:
      - id: callsign_ror
        process: ror(1)
        size: 6
        type: callsign
  callsign:
    seq:
      - id: callsign
        type: str
        encoding: ASCII
        size: 6
        valid:
          any-of:
            - '"IS-1  "'
            - '"BCT   "'
  ssid_mask:
    seq:
      - id: ssid_mask
        type: u8
  i_frame:
    seq:
      - id: pid
        type: u1
      - id: ax25_info
        type: ax25_info_data
        size-eos: true
  #ui_frame:
    #seq:
      #- id: pid
      #  type: u1
      #- id: ax25_info
      #  type: ax25_info_data
      #  size-eos: true
  ax25_info_data:
    seq:
      - id: ccsds_space_packet
        type: ccsds_space_packet_t
  ccsds_space_packet_t:
    seq:
      - id: packet_primary_header
        type: packet_primary_header_t
        size: 6
      - id: data_section
        type: data_section_t
  packet_primary_header_t:
    seq:
      - id: ccsds_version
        type: b3
      - id: packet_type
        type: b1
      - id: secondary_header_flag
        type: b1
      - id: is_stored_data
        type: b1
      - id: application_process_id
        type: b10
      - id: grouping_flag
        type: b2
      - id: sequence_count
        type: b14
      - id: packet_length
        type: u2
  data_section_t:
    seq:
      - id: user_data_field
        type: user_data_field_t
          
  user_data_field_t:
    doc: |
      This s the enclosed packet containting the various data
    seq:
      - id: shcoarse
        type: u4
        doc: |
          Seconds count
      - id: shfine
        type: u2
        doc: |
          Subsecond count
      - id: cmd_recv_count
        type: u1
        doc: |
          Command receive count
      - id: cmd_fail_count
        type: u1
        doc: |
          Command reject count
      - id: cmd_succ_count
        type: u1
        doc: |
          Command success count
      - id: cmd_succ_op
        type: u1
        doc: |
          d8 It is a decimal converted code for Command successful opcode
      - id: cmd_fail_op
        type: u1
        doc: |
          d8 it is a decimal converted code for Command failed opcode
      - id: cmd_fail_code
        type: u1
        doc: |
          d8 it is a decimal converted code for Command fail code
      - id: pwr_status
        type: u1
        doc: |
          8 bit data Power status flags
      - id: eclipse_state
        type: b1
        doc: |
          1 bit data on Eclipse State of ADCS
      - id: pwr_status_sd1
        type: b1
        doc: |
          1 bit data on Eclipse State of SD Card 1 Power State
      - id: pwr_status_sd0
        type: b1
        doc: |
         1 bit data on Eclipse State of SD Card 0 Power State
      - id: pwr_status_htr
        type: b1
        doc: |
          1 bit data on Eclipse State of Battery Heater Power State
      - id: pwr_status_sband
        type: b1
        doc: |
          1 bit data on Eclipse State of SBAND Power State
      - id: pwr_status_adcs
        type: b1
        doc: |
          1 bit data on Eclipse State of ADCS Power State
      - id: pwr_status_cip
        type: b1
        doc: |
          1 bit data on Eclipse State of CIP Power State
      - id: pwr_status_daxss
        type: b1
        doc: |
          DAXSS Power State
      - id: sd_read_misc
        type: u4
        doc: |
          Partition read pointer - misc
      - id: sd_read_scic
        type: u4
        doc: |
          Partition read pointer - scic
      - id: sd_read_scid
        type: u4
        doc: |
          Partition read pointer - scid
      - id: sd_read_adcs
        type: u4
        doc: |
          Partition read pointer - adcs
      - id: sd_read_beacon
        type: u4
        doc: |
          Partition read pointer - beacon
      - id: sd_read_log
        type: u4
        doc: |
          Partition read pointer - log
      - id: sd_write_misc
        type: u4
        doc: |
          Partition write pointer - misc
      - id: sd_write_scic
        type: u4
        doc: |
          Partition write pointer - scic
      - id: sd_write_scid
        type: u4
        doc: |
          Partition write pointer - scid
      - id: sd_write_adcs
        type: u4
        doc: |
          Partition write pointer - adcs
      - id: sd_write_beacon
        type: u4
        doc: |
          Partition write pointer - beacon
      - id: sd_write_log
        type: u4
        doc: |
          Partition write pointer - log
      - id: cmd_loss_timer
        type: u4
        doc: |
          Seconds until command loss timer
      - id: alive_flags
        type: u1
        doc: |
          Subsytem aliveness flags
      - id: clt_state
        type: b1
        doc: |
          State of CLT Fault
      - id: alive_daxss
        type: b1
        doc: |
          DAXSS Aliveness State
      - id: alive_cip
        type: b1
        doc: |
          CIP Aliveness State
      - id: alive_adcs
        type: b1
        doc: |
          ADCS Aliveness State
      - id: alive_sband
        type: b1
        doc: |
          SBAND Aliveness State
      - id: alive_uhf
        type: b1
        doc: |
          UHF Aliveness State
      - id: alive_sd1
        type: b1
        doc: |
          SD Card 1 Aliveness State
      - id: alive_sd0
        type: b1
        doc: |
          SD Card 0 Aliveness State
      - id: cip_comstat
        type: u4
        doc: |
          CIP com stat field
      - id: cip_temp1
        type: u2
        doc: |
          integer value 16 bit long CIP temp 1 conversions C0=0.000000 C1=0.007813
      - id: cip_temp2
        type: u2
        doc: |
          integer value 16 bit long CIP temp 2 conversions C0=0.000000 C1=0.007813
      - id: cip_temp3
        type: u2
        doc: |
          integer value 16 bit long CIP temp 3 conversions C0=0.000000 C1=0.007813
      - id: uhf_temp_buff
        type: b2
        doc: |
          Contains the 
      - id: uhf_temp_1
        type: b6
        doc: |
          UHF Temp part 2
      - id: uhf_config
        type: u1
        doc: |
          UHF Temp part 3
      - id: uhf_locked
        type: b1
        doc: |
          UHF Locked Mode 0/OFF 1/ON 
      - id: uhf_readback
        type: b2
        doc: |
          UHF Readback Mode 0/N 1/B 2/T 
      - id: uhf_swd
        type: b1
        doc: |
          UHF SWD Mode 0/OFF 1/ON 
      - id: uhf_afc
        type: b1
        doc: |
          UHF AFC Mode 0/OFF 1/ON 
      - id: uhf_echo
        type: b1
        doc: |
          UHF Echo Mode 0/OFF 1/ON 
      - id: uhf_channel
        type: b2
        doc: |
          UHF Channel 0/A 1/B 2/D 
      - id: sband_pa_curr
        type: u2
        doc: |
          SBand Power Amplifier Current conversion curve fits C0=0.000000 C1=0.000040 
      - id: sband_pa_volt
        type: u2
        doc: |
          SBand Power Amplifier Voltage conversion curve fits C0=0.000000 C1=0.004000
      - id: sband_rf_pwr
        type: u2
        doc: |
          SBand RF Power conversion curve fits C0=0.000000 C1=0.001139 
      - id: sband_pa_temp
        type: u2
        doc: |
          SBand Power Amplifier Temp conversion curve fits C0=-50.000000 C1=0.073242 
      - id: sband_top_temp
        type: u2
        doc: |
          SBAND Board Top Temperature conversion curve fits C0=0.000000 C1=0.062500 
      - id: sband_bottom_temp
        type: u2
        doc: |
          SBAND Board Bottom Temperature conversion curve fits C0=0.000000 C1=0.062500 
      - id: adcs_cmd_acpt
        type: u1
        doc: |
          ADCS Command Accept Count
      - id: adcs_cmd_fail
        type: u1
        doc: |
          ADCS Command Reject Count
      - id: adcs_time
        type: u4
        doc: |
          ADCS GPS Time in integer format i32
      - id: adcs_info
        type: u1
        doc: |
          ADCS Info
      - id: adcs_att_valid
        type: b1
        doc: |
          ADCS Attitude Valid
      - id: adcs_refs_valid
        type: b1
        doc: |
          ADCS Refs Valid
      - id: adcs_time_valid
        type: b1
        doc: |
          ADCS Time Valid
      - id: adcs_mode
        type: b1
        doc: |
          ADCS Mode 0/SUN 1/FINE 
      - id: adcs_recom_sun_pt
        type: b1
        doc: |
          ADCS Recommed Sun Point 0/NO 1/YES 
      - id: adcs_sun_pt_state
        type: b3
        doc: |
          ADCS Sun Point State 0/UNDEF 1/UNDEF 2/SEARCH_INIT 3/SEARCHING 4/WAITING 5/CONVERGING 6/ON_SUN 7/NOT_ACTIVE 
      - id: adcs_star_temp
        type: u1
        doc: |
          ADCS Star Track Temp 
          Conversion Curve fits: C0=0.000000 C1=0.800000 
      - id: adcs_wheel_temp1
        type: u2
        doc: |
          ADCS Wheel Temp 1
          Conversion Curve fits: C0=0.000000 C1=0.005000 
      - id: adcs_wheel_temp2
        type: u2
        doc: |
          ADCS Wheel Temp 2
          Conversion Curve fits: C0=0.000000 C1=0.005000 
      - id: adcs_wheel_temp3
        type: u2
        doc: |
          ADCS Wheel Temp 3
          Conversion Curve fits: C0=0.000000 C1=0.005000 
      - id: adcs_digi_bus_volt
        type: u2
        doc: |
          ADCS Digi Bus Voltage 
          Conversion Curve fits: C0=0.000000 C1=0.001250 
      - id: adcs_sun_vec1
        type: u2
        doc: |
          ADCS Sun Vector 1
          Conversion Curve fits: C0=0.000000 C1=0.000100 
      - id: adcs_sun_vec2
        type: u2
        doc: |
          ADCS Sun Vector 2
          Conversion Curve fits: C0=0.000000 C1=0.000100 
      - id: adcs_sun_vec3
        type: u2
        doc: |
          ADCS Sun Vector 3
          Conversion Curve fits: C0=0.000000 C1=0.000100 
      - id: adcs_wheel_sp1
        type: u2
        doc: |
          ADCS Wheel Speed 1
          Conversion Curve fits: C0=0.000000 C1=0.400000  
      - id: adcs_wheel_sp2
        type: u2
        doc: |
          ADCS Wheel Speed 2
          Conversion Curve fits: C0=0.000000 C1=0.400000
      - id: adcs_wheel_sp3
        type: u2
        doc: |
          ADCS Wheel Speed 3
          Conversion Curve fits: C0=0.000000 C1=0.400000
      - id: adcs_body_rt1
        type: u4
        doc: |
          Integer value i32 ADCS Body Frame Rate 1
      - id: adcs_body_rt2
        type: u4
        doc: |
          Integer value i32 ADCS Body Frame Rate 2
      - id: adcs_body_rt3
        type: u4
        doc: |
          Integer value i32 ADCS Body Frame Rate 3
      - id: pad1
        type: u4
        doc: |
          
      - id: pad2
        type: u4
        doc: |
          
      - id: pad3
        type: u4
        doc: |
          
      - id: daxss_time_sec
        type: u4
        doc: |
          DAXSS Time Stamp (seconds)
      - id: daxss_cmd_op
        type: u1
        doc: |
          DAXSS Command Last opcode
      - id: daxss_cmd_succ
        type: u1
        doc: |
          DAXSS Command Accept Count
      - id: daxss_cmd_fail
        type: u1
        doc: |
          DAXSS Command Reject Count
      - id: daxss_cdh_enables
        type: u2
        doc: |
          DAXSS CDH Enables
      - id: daxss_cdh_temp
        type: u2
        doc: |
          DAXSS CDH Temp (integer format i16)
      - id: daxss_sps_rate
        type: u4
        doc: |
          DAXSS SPS Sum Rate
      - id: daxss_sps_x
        type: u2
        doc: |
          DAXSS SPS X
      - id: daxss_sps_y
        type: u2
        doc: |
          DAXSS SPS Y
      - id: daxss_slow_count
        type: u2
        doc: |
          DAXSS X Slow Count Rate
      - id: bat_fg1
        type: u2
        doc: |
          Battery Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.003906 
      - id: daxss_curr
        type: u2
        doc: |
          DAXSS Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: daxss_volt
        type: u2
        doc: |
          DAXSS Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: cdh_curr
        type: u2
        doc: |
          CDH Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: cdh_volt
        type: u2
        doc: |
          CDH Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: sband_curr
        type: u2
        doc: |
          SBand Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: sband_volt
        type: u2
        doc: |
          SBand Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: uhf_curr
        type: u2
        doc: |
          UHF Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: uhf_volt
        type: u2
        doc: |
          UHF Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: heater_curr
        type: u2
        doc: |
          Battery Heater Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: heater_volt
        type: u2
        doc: |
          Battery Heater Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: sp2_curr
        type: u2
        doc: |
          SP2 Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: sp2_volt
        type: u2
        doc: |
          SP2 Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: sp1_curr
        type: u2
        doc: |
          SP1 Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: sp1_volt
        type: u2
        doc: |
          SP1 Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: sp0_curr
        type: u2
        doc: |
          SP0 Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: sp0_volt
        type: u2
        doc: |
          SP0 Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000 
      - id: bat_vcell
        type: u2
        doc: |
          Battery FG Voltage
      - id: gps_12v_2_curr
        type: u2
        doc: |
          GPS 12V_2 Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: gps_12v_2_volt
        type: u2
        doc: |
          GPS 12V_2 Voltage 
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: gps_12v_1_curr
        type: u2
        doc: |
          GPS 12V_1 Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: gps_12v_1_volt
        type: u2
        doc: |
          GPS 12V_1 Voltage 
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: bat_curr
        type: u2
        doc: |
          Battery Sense Curr
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: bat_volt
        type: u2
        doc: |
          Battery Sense Voltage 
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: adcs_curr
        type: u2
        doc: |
          ADCS Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: adcs_volt
        type: u2
        doc: |
          ADCS Voltage 
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: curr_3p3
        type: u2
        doc: |
          3.3 Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: volt_3p3
        type: u2
        doc: |
          3.3 Voltage 
          Conversion Curve Fits: C0=0.000000 C1=0.001000
      - id: cip_curr
        type: u2
        doc: |
          CIP Current
          Conversion Curve Fits: C0=0.000000 C1=0.000500 
      - id: cip_volt
        type: u2
        doc: |
          CIP Voltage
          Conversion Curve Fits: C0=0.000000 C1=0.001000 
      - id: obc_temp
        type: u2
        doc: |
          OBC Temperature 1 
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: eps_temp
        type: u2
        doc: |
          EPS Temperature 
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: int_temp
        type: u2
        doc: |
          Interface Temperature 
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: sp0_temp
        type: u2
        doc: |
          SP0 Temperature
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: bat0_temp
        type: u2
        doc: |
          Battery 0 Temperature
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: sp1_temp
        type: u2
        doc: |
          SP1 Temperature
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: bat1_temp
        type: u2
        doc: |
          Battery 1 Temperature
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000
      - id: sp2_temp
        type: u2
        doc: |
          SP2 Temperature
          Conversion Curve Fits: C0=91.394000 C1=-0.089493 C2=0.000036 C3=-0.000000 C4=0.000000 
      - id: bat_fg3
        type: u2
        doc: |
          Battery Gauge 
          Conversion Curve Fits: C0=0.000000 C1=0.003906
      - id: bat0_temp_conv
        type: f4
        doc: |
          Battery Temp 0 Converted
      - id: bat1_temp_conv
        type: f4
        doc: |
          Battery Temp 1 Converted
      - id: mode
        type: u1
        doc: |
          Current system mode 0/PHOENIX 1/SAFE 2/SCID 3/SCIC 
